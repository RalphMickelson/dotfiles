# =========================================== #
# Config file for Phoenix OS X Window Manager #
# =========================================== #
hList = []; # keep the handlers, or nothing works
modKeys = ['cmd', 'alt']

bind = (key, func) -> hList.push Phoenix.bind key, modKeys, func
bind3 = (key, keys, func) -> hList.push Phoenix.bind key, keys, func
log = (x) -> Phoenix.log x

# Enable these to assist with testing
# bind 'r', -> reload()
# bind 'z', -> log 'This is a test'

# maximize window
bind 'f', ->
  win = Window.focusedWindow()
  win.setFrame win.screen().visibleFrameInRectangle()

# maximize window on main screen (good for finding lost windows)
bind "m", ->
  Window.focusedWindow().setFrame Screen.mainScreen().visibleFrameInRectangle()

# center on screen
bind 'c', ->
  win = Window.focusedWindow()
  sRect = win.screen().visibleFrameInRectangle()
  wRect = win.frame()
  wRect.x = sRect.x + (sRect.width - wRect.width) / 2
  wRect.y = sRect.y + (sRect.height - wRect.height) / 2
  win.setFrame wRect

# =========== CMD + ALT === Scale by 1/2 ============================

bind "left", ->
  win = Window.focusedWindow()
  sRect = win.screen().visibleFrameInRectangle()
  wRect = win.frame()
  return lft 1/2 if wRect.x != sRect.x
  setWin { w: 1/2 }, wRect

bind "up", ->
  win = Window.focusedWindow()
  sRect = win.screen().visibleFrameInRectangle()
  wRect = win.frame()
  return top 1/2 if wRect.y != sRect.y
  setWin { h: 1/2 }, wRect

bind "right", ->
  sRect = getScr()
  wRect = getWin().frame()
  sRgt = sRect.x + sRect.width
  wRgt = wRect.x + wRect.width
  return rgt 1/2 if Math.abs(wRgt - sRgt) > 5 # fuzzy logic for right edge
  setWin { w: 1/2, x: 1/2 }, wRect

bind "down", ->
  sRect = getScr()
  wRect = getWin().frame()
  sBot = sRect.y + sRect.height
  wBot = wRect.y + wRect.height
  return bot 1/2 if Math.abs(wBot - sBot) > 20 # fuzzy logic of bottom edge
  setWin { h: 1/2, y: 1/2 }, wRect

# =========== CMD + ALT + SHFT ======= Drag ========================
dragModKeys = modKeys.concat "shift"
bind3 "left" , dragModKeys, -> setWin { x:-1 }, getWin().frame()
bind3 "right", dragModKeys, -> setWin { x: 1 }, getWin().frame()
bind3 "up"   , dragModKeys, -> setWin { y:-1 }, getWin().frame()
bind3 "down" , dragModKeys, -> setWin { y: 1 }, getWin().frame()

# =========== Throw to other screen ============
bind ',', ->
  win = getWin()
  win.setFrame win.screen().previous().visibleFrameInRectangle()

bind '.', ->
  win = getWin()
  win.setFrame win.screen().next().visibleFrameInRectangle()

# =============== Support Functions ===================
top = (h) -> setWin { h: h }
bot = (h) -> setWin { h: h, y: 1-h }
lft = (w) -> setWin { w: w }
rgt = (w) -> setWin { w: w, x: 1-w }
topLft = (w, h) -> setWin { w: w, h: h }
botLft = (w, h) -> setWin { w: w, h: h, y: 1-h }
topRgt = (w, h) -> setWin { w: w, h: h, x: 1-w }
botRgt = (w, h) -> setWin { w: w, h: h, x: 1-w, y: 1-h }

# Returns a Window object of current focued window
getWin = () -> Window.focusedWindow()

# Returns a Rect(origin, size) of current screen
getScr = () -> getWin().screen().visibleFrameInRectangle()

# Sets the current window size/loc based on mult(w,h,x,y);  all optional attr
# An optional reference rectangle may be passed; defaults to the screen
setWin = (mult, r) ->
  win = Window.focusedWindow()
  r ||= win.screen().visibleFrameInRectangle()

  # Change the position by a muliple of the 'prior' size
  # (multiplier may be fractional, positive or negative)
  r.x += Math.floor(r.width  * mult.x) if mult.x
  r.y += Math.floor(r.height * mult.y) if mult.y

  # Change the size by a multiplier
  r.width  = Math.floor(r.width  * mult.w) if mult.w
  r.height = Math.floor(r.height * mult.h) if mult.h

  win.setFrame r

modal = new Modal();
modal.message = 'Phoenix Script File Loaded';
modal.duration = 5;
modal.show();
