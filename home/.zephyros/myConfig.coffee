bind "T", baseKeyMods, ->
  cw = getWin()
  for app in api.runningApps()
    log app.title()
    if String(app.title()) is 'Zephyros'
      for w in app.visibleWindows()
        log '==>'+w.frame().origin.x+','+w.frame().origin.y+','+w.frame().size.width+','+w.frame().size.height

##################################################
findWin = (name, min=-99999, max=99999) ->
  isFocused = name is String(api.focusedWindow().app().title())
  for app in api.runningApps()
    if String(app.title()) is name
      for win in app.visibleWindows()
        continue unless min <= win.frame().origin.x <= max
        win.focusWindow()
        return [win, isFocused, app]

# emacs window
bind "pad.", baseKeyMods, ->
  [win] = findWin 'Zephyros'
  f = win.frame()
  f.origin.x = 100000
  f.origin.y = 100000
  win.setFrame f

# emacs window
bind "pad_enter", baseKeyMods, ->
  [win, wasFocused] = findWin 'Emacs'
  return unless wasFocused
  sRect = win.screen().frameWithoutDockOrMenu()
  wRect = win.frame()
  w = sRect.size.width * 3/4
  h = sRect.size.height * 2/3
  diff = Math.abs(wRect.size.width-w) + Math.abs(wRect.size.height-h)
  return botRgt 3/4, 2/3 if diff > 30
  botRgt 3/5, 1/3

# terminal window
bind "pad0", baseKeyMods, ->
  [win, wasFocused] = findWin 'Terminal'
  return unless wasFocused
  sRect = win.screen().frameWithoutDockOrMenu()
  wRect = win.frame()
  w = sRect.size.width * 1/4
  h = sRect.size.height
  diff = Math.abs(wRect.size.width-w) + Math.abs(wRect.size.height-h)
  return lft 1/4  if diff > 30
  lft 1/2

# chrome work window
bind "pad9", baseKeyMods, ->
  [win, wasFocused, app] = findWin 'Google Chrome', 0, 3000
  return unless wasFocused
  sRect = win.screen().frameWithoutDockOrMenu()
  wRect = win.frame()
  w = sRect.size.width * 1/3
  h = sRect.size.height * 1/3
  diff = Math.abs(wRect.size.width-w) + Math.abs(wRect.size.height-h)
  if diff > 30
    sRect.size.height /= 3
    sRect.size.width  /= 3
    sRect.origin.x += sRect.size.width *2
    win.setFrame sRect
  else
    wRect.size.height = sRect.size.height *2/3
    wRect.origin.x = sRect.size.width / 2
    wRect.size.width = sRect.size.width / 2
    win.setFrame wRect
  win.focusWindow()

# finder window
bind "pad8", baseKeyMods, ->
  [win, wasFocused] = findWin 'Finder'
  return unless wasFocused
  sRect = win.screen().frameWithoutDockOrMenu()
  wRect = win.frame()
  w = 2000
  h = sRect.size.height/3
  diff = Math.abs(wRect.size.width-w) + Math.abs(wRect.size.height-h)
  sRect.origin.x = sRect.size.width/4
  sRect.size.width = w
  if diff > 30
    sRect.size.height = h
    win.setFrame sRect
  else
    sRect.size.height = h*2
    win.setFrame sRect

# SQL window
bind "pad/", baseKeyMods, ->
  [win, wasFocused] = findWin 'MySQLWorkbench'
  return unless wasFocused
  sRect = win.screen().frameWithoutDockOrMenu()
  wRect = win.frame()
  w = 2000
  h = sRect.size.height/3
  diff = Math.abs(wRect.size.width-w) + Math.abs(wRect.size.height-h)
  sRect.origin.x = sRect.size.width/4
  sRect.size.width = w
  if diff > 30
    sRect.size.height = h
    win.setFrame sRect
  else
    sRect.size.height = h*2
    win.setFrame sRect

# chrome other window
bind "pad+", baseKeyMods, ->
  [win, wasFocused, app] = findWin 'Google Chrome', 3800
  return unless wasFocused
  sRect = win.screen().frameWithoutDockOrMenu()
  wRect = win.frame()
  wRect.origin.x = 3875
  wRect.origin.y = sRect.origin.y
  wRect.size.width = 1235
  wRect.size.height = wRect.size.height
  win.setFrame wRect

# sqwiggle window
bind "pad-", baseKeyMods, ->
  [win, wasFocused, app] = findWin 'Sqwiggle'
  wRect = win.frame()
  wRect.origin.x = 4555
  wRect.origin.y = 815
  wRect.size.width = 725
  wRect.size.height =800
  win.setFrame wRect


# bind "pad+", baseKeyMods, ->
#   (win = findWin 'Emacs').focusWindow()
#   frame = win.screen().frameWithoutDockOrMenu()
#   frame.origin.x += frame.size.width * 1/4
#   frame.origin.y += frame.size.height * 1/3
#   frame.size.width *= 3/4
#   frame.size.height *= 2/3
#   win.setFrame frame
