# ============================================== #
# My extra stuff for Phoenix OS X Window Manager #
# ============================================== #
#[sXOfs,sYOfs] = [-1440,1400]
[sXOfs,sYOfs] = [3840,817]

bind 't', ->
  log '<==========Apps===========>'
  cw = getWin()
  for app in App.runningApps()
    continue unless app.visibleWindows().length>0
    log app.name()
    for w in app.visibleWindows()
      f = w.frame()
      log '==>'+f.x+','+f.y+' '+f.width+','+f.height

##################################################
findWin = (title, min=-99999, max=99999) ->
  win = getWin()
  if win && win.title().indexOf(title) >=0
    return [win, true]
  for win in Window.visibleWindows()
    if win.title().indexOf(title) >=0
      continue unless min <= win.frame().x <= max
      win.focus()
      return [win, false]
  return [null]

findAppWin = (name, min=-99999, max=99999) ->
  win = getWin()
  if win && win.app().name().indexOf(name) >=0
    return [win, true, win.app()]
  for app in App.runningApps()
    continue if app.name().indexOf(name) <0
    for win in app.visibleWindows()
      continue unless min <= win.frame().x <= max
      win.focus()
      return [win, false, app]
  [null]
##################################################
prep = (name, setup) ->
  log name
  [win, wasFocused] = findAppWin name
  log win
  return App.launch(name) unless win
  log setup
  log wasFocused
  return [] unless setup || wasFocused
  log 'wasFocused'
  [win.screen().visibleFrameInRectangle(), win.frame(), win]

apps = {}
apps.terminal = (setup) ->
  [sRect, wRect] = prep 'Terminal', setup
  return unless wRect
  w = sRect.width * 1/4
  h = sRect.height
  diff = Math.abs(wRect.width-w) + Math.abs(wRect.height-h)
  return lft 1/4  if setup || diff > 30
  lft 1/2
apps.emacs = (setup) ->
  [sRect, wRect] = prep 'Emacs', setup
  log sRect
  log wRect
  return unless wRect
  w = sRect.width * 3/4
  h = sRect.height * 2/3
  diff = Math.abs(wRect.width-w) + Math.abs(wRect.height-h)
  return botRgt 3/4, 2/3 if setup || diff > 30
  botRgt 3/5, 1/3
apps.topCenter = (name, setup, ofs) ->
  [sRect, wRect, win] = prep name, setup
  return unless wRect
  w = 2000
  h = sRect.height/3
  diff = Math.abs(wRect.width-w) + Math.abs(wRect.height-h)
  sRect.x = sRect.width/4 + ofs
  sRect.width = w
  sRect.height = if diff > 30 then h else h*2
  win.setFrame sRect
apps.mySQL = (setup) -> apps.topCenter 'MySQLWorkbench', setup, 40
apps.finder = (setup) -> apps.topCenter 'Finder', setup, 0
apps.speak = (setup) ->
  [sRect, wRect, win] = prep 'Speak', setup
  log wRect
  return unless wRect
  wRect.x = sXOfs
  wRect.y = sYOfs
  win.setFrame wRect
apps.slack = (setup) ->
  [sRect, wRect, win] = prep 'Slack', setup
  log wRect
  return unless wRect
  wRect.x = sXOfs
  wRect.y = sYOfs + (875 - wRect.height)
  log wRect.x
  log wRect.y
  win.setFrame wRect

bind 'keypadEnter', apps.emacs
bind "keypad0"    , apps.terminal
bind "keypad/"    , apps.mySQL
bind "keypad8"    , apps.finder

# chrome work window
bind "keypad9", ->
  [win, wasFocused] = findWin 'AllyConnect', 0, 3000
  return unless win && wasFocused
  sRect = win.screen().visibleFrameInRectangle()
  wRect = win.frame()
  w = sRect.width * 1/3
  h = sRect.height * 1/3
  diff = Math.abs(wRect.width-w) + Math.abs(wRect.height-h)
  if diff > 30
    sRect.height /= 3
    sRect.width  /= 3
    sRect.x += sRect.width *2
    win.setFrame sRect
  else
    wRect.height = sRect.height *2/3
    wRect.x = sRect.width / 2
    wRect.width = sRect.width / 2
    win.setFrame wRect
  win.focusWindow()

# chrome other window
bind "keypad+", ->
  [win, wasFocused] = findWin 'Google Chrome', 3800
  return unless win && wasFocused
  sRect = win.screen().visibleFrameInRectangle()
  wRect = win.frame()
  wRect.x = 3875
  wRect.y = sRect.y
  wRect.width = 1235
  wRect.height = sRect.height
  win.setFrame wRect

# setup all windows
bind "keypad-", ->
  apps.slack true
  log '**'
  apps.emacs true
  log '1'
  apps.terminal true
  log '2'
  apps.mySQL true
  log '3'
  apps.finder true
  log '##'
  apps.speak true
# bind "pad+", ->
#   (win = findWin 'Emacs').focusWindow()
#   frame = win.screen().visibleFrameInRectangle()
#   frame.x += frame.width * 1/4
#   frame.y += frame.height * 1/3
#   frame.width *= 3/4
#   frame.height *= 2/3
#   win.setFrame frame
