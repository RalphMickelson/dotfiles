bind "T", ["cmd", "alt"], ->
  cw = getWin()
  for app in api.runningApps()
    log app.title()
    if String(app.title()) is 'Google Chrome'
      for w in app.visibleWindows()
        log '==>'+w.title()
