(load (expand-file-name "optional/cua-plus.el" prelude-personal-dir))
(setq mac-allow-anti-aliasing 1)
;;(set-face-attribute 'default nil :weight 'normal :height 130 :width 'normal :foundry "apple" :family "andale mono") ;85,91 (x,y based on my std 4 pane setup, 130 ht)
;;(set-face-attribute 'default nil :weight 'normal :height 130 :width 'normal :foundry "apple" :family "menlo") ;85,91
;;(set-face-attribute 'default nil :weight 'normal :height 130 :width 'normal :foundry "apple" :family "Monaco") ;85,80
;;(set-face-attribute 'default nil :weight 'normal :height 130 :width 'normal :foundry "apple" :family "gill sans") ;96,91
;;(set-face-attribute 'default nil :weight 'normal :height 130 :width 'normal :foundry "apple" :family "arial") ;97,91
;;(set-face-attribute 'default nil :weight 'normal :height 130 :width 'normal :foundry "apple" :family "arial narrow") ;113,91
;;(set-face-attribute 'default nil :weight 'normal :height 130 :width 'normal :foundry "apple" :family "osaka") ;75,76
(set-face-attribute 'default nil :weight 'normal :height 160 :width 'normal :foundry "apple" :family "inconsolata") ;96,98
;;(set-face-attribute 'default nil :weight 'normal :height 130 :width 'normal :foundry "apple" :family "pt mono") ;85,91

(global-linum-mode)
(global-set-key [C-M-f13] 'global-linum-mode)
;;(setq linum-delay t)
;; (setq linum-format "%3d")
;; (set-fringe-mode 0)
(setq scroll-conservatively 10000)

(global-unset-key [M-return] )
(add-hook 'cua-mode-hook
          '(lambda ()
             (define-key cua--rectangle-keymap (kbd "C-S-<return>")
               'cua-clear-rectangle-mark)
             (define-key cua--region-keymap (kbd "C-S-<return>")
               'cua-toggle-rectangle-mark)
             (define-key cua-global-keymap (kbd "C-S-<return>")
               'cua-set-rectangle-mark)
             (define-key cua--rectangle-keymap [(control return)] nil)
             (define-key cua--region-keymap [(control return)] nil)
             (define-key cua-global-keymap [(control return)] nil)
             )
          )

(global-set-key [M-s-left]  'previous-buffer)
(global-set-key [M-s-right] 'next-buffer)
(global-set-key [f9]        'revert-buffer)
(global-set-key [M-f9]      'revert-all-buffers)

(global-set-key [C-return]  (lambda () (interactive)  ;; force save
                              (set-buffer-modified-p t) (save-buffer 0)))
(global-set-key [C-M-return] (lambda () (interactive) ;; save & close
                               (save-buffer 0) (kill-buffer)))
(global-set-key [C-M-kp-delete] (lambda () (interactive) (kill-buffer))) ;; close

(global-set-key (kbd "C-S-k") (lambda () (interactive) (delete-horizontal-space)))
(global-set-key (kbd "C-l") (lambda () (interactive) (kill-whole-line)))
(global-set-key (kbd "C-j") (lambda () (interactive) (let ((x (point)))
                             (smart-beginning-of-line) (kill-region x (point)))))

(global-set-key [C-tab] 'ibuffer)
;;(define-key enh-ruby-mode-map (kbd "C-M-f") nil)
(global-set-key (kbd "C-M-f") (lambda () (interactive) (helm-projectile-find-file)))
(global-set-key [(super tab)]  (lambda () (interactive) (ido-switch-buffer)))

(global-set-key (kbd "C-S-z") 'undo-tree-redo)

;; (global-set-key (kbd "C-S-M-p") "\M-<\C-n// CHECKSTYLE OFF: ALL\C-mimport static com.allydvm.Utils.*;")
;;(global-set-key [f14] 'compile c++ -std=c+11 -c *)
(global-set-key [f13] 'previous-error)
(global-set-key [f15] 'next-error)

(add-hook 'c-mode-common-hook
          (lambda()
            (local-set-key  (kbd "s-`") 'ff-find-other-file)))
;; Reload .emacs file by typing: M-x reload.
(defun reload () "Reloads .emacs interactively."
       (interactive)
       (load "~/.emacs.d/init.el")
       )
;; Edit this .emacs file by typing: M-x edit-personal.
(defun edit-personal () "edit my personal.el."
       (interactive)
       (find-file "~/.emacs.d/personal/personal.el")
       )

(load (expand-file-name "personal/dt.el" prelude-personal-dir))

;; set keys for Apple keyboard (Mac OS), so emacs matches other OS X editors
(setq mac-command-modifier 'control)
(setq mac-option-modifier 'super)
(setq mac-control-modifier 'meta)
(setq ns-function-modifier 'hyper)

(provide 'personal)
;;; personal.el ends here
