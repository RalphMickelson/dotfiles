(function() {
  var apps, findAppWin, findWin, prep, ref, sXOfs, sYOfs;

  ref = [3840, 817], sXOfs = ref[0], sYOfs = ref[1];

  bind('t', function() {
    var app, cw, f, i, len, ref1, results, w;
    log('<==========Apps===========>');
    cw = getWin();
    ref1 = App.runningApps();
    results = [];
    for (i = 0, len = ref1.length; i < len; i++) {
      app = ref1[i];
      if (!(app.visibleWindows().length > 0)) {
        continue;
      }
      log(app.name());
      results.push((function() {
        var j, len1, ref2, results1;
        ref2 = app.visibleWindows();
        results1 = [];
        for (j = 0, len1 = ref2.length; j < len1; j++) {
          w = ref2[j];
          f = w.frame();
          results1.push(log('==>' + f.x + ',' + f.y + ' ' + f.width + ',' + f.height));
        }
        return results1;
      })());
    }
    return results;
  });

  findWin = function(title, min, max) {
    var i, len, ref1, ref2, win;
    if (min == null) {
      min = -99999;
    }
    if (max == null) {
      max = 99999;
    }
    win = getWin();
    if (win && win.title().indexOf(title) >= 0) {
      return [win, true];
    }
    ref1 = Window.visibleWindows();
    for (i = 0, len = ref1.length; i < len; i++) {
      win = ref1[i];
      if (win.title().indexOf(title) >= 0) {
        if (!((min <= (ref2 = win.frame().x) && ref2 <= max))) {
          continue;
        }
        win.focus();
        return [win, false];
      }
    }
    return [null];
  };

  findAppWin = function(name, min, max) {
    var app, i, j, len, len1, ref1, ref2, ref3, win;
    if (min == null) {
      min = -99999;
    }
    if (max == null) {
      max = 99999;
    }
    win = getWin();
    if (win && win.app().name().indexOf(name) >= 0) {
      return [win, true, win.app()];
    }
    ref1 = App.runningApps();
    for (i = 0, len = ref1.length; i < len; i++) {
      app = ref1[i];
      if (app.name().indexOf(name) < 0) {
        continue;
      }
      ref2 = app.visibleWindows();
      for (j = 0, len1 = ref2.length; j < len1; j++) {
        win = ref2[j];
        if (!((min <= (ref3 = win.frame().x) && ref3 <= max))) {
          continue;
        }
        win.focus();
        return [win, false, app];
      }
    }
    return [null];
  };

  prep = function(name, setup) {
    var ref1, wasFocused, win;
    log(name);
    ref1 = findAppWin(name), win = ref1[0], wasFocused = ref1[1];
    log(win);
    if (!win) {
      return App.launch(name);
    }
    log(setup);
    log(wasFocused);
    if (!(setup || wasFocused)) {
      return [];
    }
    log('wasFocused');
    return [win.screen().visibleFrameInRectangle(), win.frame(), win];
  };

  apps = {};

  apps.terminal = function(setup) {
    var diff, h, ref1, sRect, w, wRect;
    ref1 = prep('Terminal', setup), sRect = ref1[0], wRect = ref1[1];
    if (!wRect) {
      return;
    }
    w = sRect.width * 1 / 4;
    h = sRect.height;
    diff = Math.abs(wRect.width - w) + Math.abs(wRect.height - h);
    if (setup || diff > 30) {
      return lft(1 / 4);
    }
    return lft(1 / 2);
  };

  apps.emacs = function(setup) {
    var diff, h, ref1, sRect, w, wRect;
    ref1 = prep('Emacs', setup), sRect = ref1[0], wRect = ref1[1];
    log(sRect);
    log(wRect);
    if (!wRect) {
      return;
    }
    w = sRect.width * 3 / 4;
    h = sRect.height * 2 / 3;
    diff = Math.abs(wRect.width - w) + Math.abs(wRect.height - h);
    if (setup || diff > 30) {
      return botRgt(3 / 4, 2 / 3);
    }
    return botRgt(3 / 5, 1 / 3);
  };

  apps.topCenter = function(name, setup, ofs) {
    var diff, h, ref1, sRect, w, wRect, win;
    ref1 = prep(name, setup), sRect = ref1[0], wRect = ref1[1], win = ref1[2];
    if (!wRect) {
      return;
    }
    w = 2000;
    h = sRect.height / 3;
    diff = Math.abs(wRect.width - w) + Math.abs(wRect.height - h);
    sRect.x = sRect.width / 4 + ofs;
    sRect.width = w;
    sRect.height = diff > 30 ? h : h * 2;
    return win.setFrame(sRect);
  };

  apps.mySQL = function(setup) {
    return apps.topCenter('MySQLWorkbench', setup, 40);
  };

  apps.finder = function(setup) {
    return apps.topCenter('Finder', setup, 0);
  };

  apps.speak = function(setup) {
    var ref1, sRect, wRect, win;
    ref1 = prep('Speak', setup), sRect = ref1[0], wRect = ref1[1], win = ref1[2];
    log(wRect);
    if (!wRect) {
      return;
    }
    wRect.x = sXOfs;
    wRect.y = sYOfs;
    return win.setFrame(wRect);
  };

  apps.slack = function(setup) {
    var ref1, sRect, wRect, win;
    ref1 = prep('Slack', setup), sRect = ref1[0], wRect = ref1[1], win = ref1[2];
    log(wRect);
    if (!wRect) {
      return;
    }
    wRect.x = sXOfs;
    wRect.y = sYOfs + (875 - wRect.height);
    log(wRect.x);
    log(wRect.y);
    return win.setFrame(wRect);
  };

  bind('keypadEnter', apps.emacs);

  bind("keypad0", apps.terminal);

  bind("keypad/", apps.mySQL);

  bind("keypad8", apps.finder);

  bind("keypad9", function() {
    var diff, h, ref1, sRect, w, wRect, wasFocused, win;
    ref1 = findWin('AllyConnect', 0, 3000), win = ref1[0], wasFocused = ref1[1];
    if (!(win && wasFocused)) {
      return;
    }
    sRect = win.screen().visibleFrameInRectangle();
    wRect = win.frame();
    w = sRect.width * 1 / 3;
    h = sRect.height * 1 / 3;
    diff = Math.abs(wRect.width - w) + Math.abs(wRect.height - h);
    if (diff > 30) {
      sRect.height /= 3;
      sRect.width /= 3;
      sRect.x += sRect.width * 2;
      win.setFrame(sRect);
    } else {
      wRect.height = sRect.height * 2 / 3;
      wRect.x = sRect.width / 2;
      wRect.width = sRect.width / 2;
      win.setFrame(wRect);
    }
    return win.focusWindow();
  });

  bind("keypad+", function() {
    var ref1, sRect, wRect, wasFocused, win;
    ref1 = findWin('Google Chrome', 3800), win = ref1[0], wasFocused = ref1[1];
    if (!(win && wasFocused)) {
      return;
    }
    sRect = win.screen().visibleFrameInRectangle();
    wRect = win.frame();
    wRect.x = 3875;
    wRect.y = sRect.y;
    wRect.width = 1235;
    wRect.height = sRect.height;
    return win.setFrame(wRect);
  });

  bind("keypad-", function() {
    apps.slack(true);
    log('**');
    apps.emacs(true);
    log('1');
    apps.terminal(true);
    log('2');
    apps.mySQL(true);
    log('3');
    apps.finder(true);
    log('##');
    return apps.speak(true);
  });

}).call(this);
