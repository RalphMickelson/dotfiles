(defvar dt-session-dir
  (concat (getenv "HOME") "/.emacs.d/desktop-sessions/")
  "*Directory to save desktop sessions in")

(defvar dt-session-name-hist nil
  "Desktop session name history")

(defun dt-save (&optional name)
  "Save desktop by name."
  (interactive)
  (unless name
    (setq name (dt-get-session-name "Save session" t)))
  (when name
    (make-directory (concat dt-session-dir name) t)
    (desktop-save (concat dt-session-dir name) t)))

(defun dt-save-and-clear ()
  "Save and clear desktop."
  (interactive)
  (call-interactively 'dt-save)
  (desktop-clear)
  (setq desktop-dirname nil))

(defun dt-read (&optional name)
  "Read desktop by name."
  (interactive)
  (unless name
    (setq name (dt-get-session-name "Load session")))
  (when name
    (desktop-clear)
    (desktop-read (concat dt-session-dir name))))

(defun dt-change (&optional name)
  "Change desktops by name."
  (interactive)
  (let ((name (dt-get-current-name)))
    (when name
      (dt-save name))
    (call-interactively 'dt-read)))

(defun dt-name ()
  "Return the current desktop name."
  (interactive)
  (let ((name (dt-get-current-name)))
    (if name
        (message (concat "Desktop name: " name))
      (message "No named desktop loaded"))))

(defun dt-get-current-name ()
  "Get the current desktop name."
  (when desktop-dirname
    (let ((dirname (substring desktop-dirname 0 -1)))
      (when (string= (file-name-directory dirname) dt-session-dir)
        (file-name-nondirectory dirname)))))

(defun dt-get-session-name (prompt &optional use-default)
  "Get a session name."
  (let* ((default (and use-default (dt-get-current-name)))
         (full-prompt (concat prompt (if default
                                         (concat " (default " default "): ")
                                       ": "))))
    (completing-read full-prompt (and (file-exists-p dt-session-dir)
                                      (directory-files dt-session-dir))
                     nil nil nil dt-session-name-hist default)))

;; (defun dt-kill-emacs-hook ()
;;   "Save desktop before killing emacs."
;;   (when (file-exists-p (concat dt-session-dir "last-session"))
;;     (setq desktop-file-modtime
;;           (nth 5 (file-attributes (desktop-full-file-name (concat dt-session-dir "last-session"))))))
;;   (dt-save "last-session"))

;; (add-hook 'kill-emacs-hook 'dt-kill-emacs-hook)
