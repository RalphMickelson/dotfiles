# Path to your oh-my-zsh installation.
export ZSH=/Users/ralph/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="robbyrussell"
ZSH_THEME="my"
# ZSH_THEME="gallois"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git jump emacs rvm ssh-agent)

#plugins=(git jump emacs colored-man-pages brew osx ssh-agent nyan extract bower mvn node nvm bundler rvm gem rails rake pip rand-quote)
# User configuration

export PATH="/usr/local/bin:$PATH:/Users/ralph/.jenv/shims:/usr/local/sbin:/usr/bin:/bin:/Users/ralph/bin:/usr/sbin:/sbin:/opt/X11/bin:/usr/local/share/npm/bin"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

source $HOME/.homesick/repos/homeshick/homeshick.sh

alias g++='gcc -std=c++11'

alias gt='gco -'
alias gdbc='git difftool'
alias gdd='git difftool --dir-diff'
alias gdm='gdd master'

alias myP="mysql -p'4lly#123' -uallyuser -hallysync2.cmmlrunxg6rc.us-east-1.rds.amazonaws.com"
alias runjs='mvn -P script-run -pl scripts -am compile'
alias pct='mvn -P dev-run -pl client -am test'
alias pst_old='mvn -P dev-run -pl server -am test'
alias pst='mvn -P dev-run -pl server spring-boot:run'
alias psd='mvn -P production -pl server -am clean package verify'
alias ag='alias|grep'
alias j=jump
alias cx='chmod +x'
alias pdlog='scp pulsar:/usr/share/tomcat8/debug_query.log .'
alias .t='j u;./t 1960July11|grep -i'
alias rdumpsync='mysqldump -h localhost -u root -d pulsar_test > db/sync_schema.sql'
alias rlcsv='ruby projects/utils/log2csv.rb'
alias zload='. ~/.zshrc'
alias zedit='e ~/.zshrc'
alias zclear='rm ~/.zcompdump*'
function t() { grep -i $1 ~/projects/utils/tt }
# export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
