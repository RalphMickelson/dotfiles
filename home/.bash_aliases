alias sudo='sudo '
alias be='bundle exec'

alias ec='/Applications/Emacs.app/Contents/MacOS/bin/emacsclient -n'
sec() {
    if [[ "$@" = /* ]]
    then
        # absolute path
        ec -t /sudo::"$@"
    else
        # Relative path
        ec -t /sudo::`pwd`/"$@"
    fi
}


# some more ls aliases
alias ll='ls -alFh'
alias la='ls -A'
alias l='ls -CF'
alias z='ps ax | grep '
alias zr='z rails'

# some git stuff
alias  gs='git status'
alias  ga='git add'
alias  gi='git commit -m'
alias  gp='git push'
alias  gb='git branch -a'
alias  gc='git checkout'
alias gcb='git checkout -b'
alias  gd='git diff --word-diff=color'
alias gdc='git diff --word-diff=color --cached -C'
alias gdm='git diff --word-diff=color master -C'
alias  gt='gc -'
alias gtd='git diff --word-diff=color @{-1} -C'
alias gtp='gt && git pull'
alias gtm='gt && git merge @{-1}'
alias gdbr='gt;gt;read -p"Delete last branch?" -n1 ans;echo;if [ "$ans" = "y" ]; then git branch -d @{-1};fi;gb'
alias  gu='gtp && gtm'
alias  gx='gtp && git remote prune origin && gdbr'

alias dbs='gc master db/structure.sql && RAILS_ENV=test spring rake db:drop db:create db:structure:load db:migrate'

alias msn='mysql -u root -D allynova_development'
alias mss='mysql -u root -D allysync_development'

alias bashae='ec ~/.bash_aliases'
alias bashe='ec ~/.bashrc'
alias bashld='. ~/.bashrc'
alias j='jump'

alias lf='find . -name'
alias FS="echo find . -type f -name '*.txt' -exec sed -i '' s/this/that/ {} + # The -type f is just good practice; sed will complain if you give it a directory or so. -exec is preferred over xargs; you needn't bother with -print0 or anything. The {} + at the end means that find will append all results as arguments to one instance of the called command, instead of re-running it for each result."
















# svndiff()
# {
#   svn diff "${@}" | colordiff | less -R
# }

# hgdiff()
# {
#   hg diff "${@}" | colordiff | less -R
# }

# ccMonitor() {
#   while ~/bin/sleep_until_modified.py "$1" ; do
#     echo -n 'copying...'
#     scp -q $1 js:/cygdrive/c/inetpub/wwwroot/ComputerCraft/$1.txt
#     echo ' done'
#   done
# }

# ccMonitor2() {
#   while ~/bin/sleep_until_modified.py "$1" ; do
#     echo -n 'copying...'
#     cat "$1" | ssh js "cat > /cygdrive/c/inetpub/wwwroot/ComputerCraft/$1.txt"
#     echo ' done'
#   done

# }
